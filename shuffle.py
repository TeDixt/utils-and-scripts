import os
import re
import random
import shutil
import sys
import datetime
import multiprocessing
from ctypes import c_bool

PATH = 'Music'
NEW_PATH = 'Shuffle'

FILENAME_CLEAN_REGEX = re.compile(r'^\d+\. ')


class FileCopier(multiprocessing.Process):
    def __init__(self, task_queue, running, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.task_queue = task_queue
        self.running = running

    def copy(self, old_path, new_path):
        shutil.copy2(old_path, new_path)

    def process_task(self):
        task = self.task_queue.get()
        self.copy(*task)

    def run(self):
        while self.running:
            if not self.task_queue.empty():
                self.process_task()

        # Finishing last tasks
        while not self.task_queue.empty():
            self.process_task()


class FileShuffler(multiprocessing.Process):
    def __init__(self, config, running, task_queue, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.input_dir = config.input_dir
        self.output_dir = config.output_dir
        self.files_list = os.listdir(self.input_dir)
        random.shuffle(self.files_list)
        self.processed_count = 0
        self.total_count = len(self.files_list)
        self.num_length = len(str(self.total_count))
        console_columns = shutil.get_terminal_size((80, 20))[0]
        self.console_size = min(console_columns, 120)
        self.task_queue = task_queue
        self.running = running
        self.prepare_dir()

    def prepare_dir(self):
        if self.input_dir != self.output_dir:
            try:
                os.mkdir(self.output_dir)
            except FileExistsError:
                shutil.rmtree(self.output_dir)
                os.mkdir(self.output_dir)

    def log_progress(self):
        percent = float(self.processed_count) / self.total_count
        arrow_string = '█' * int(round(percent * self.console_size))
        spaces = '-' * (self.console_size - len(arrow_string))
        progress = arrow_string + spaces
        percent = round(percent * 100, 1)
        sys.stdout.write("\rProgress: [{0}] {1}%".format(progress, percent))
        sys.stdout.flush()

    def process_file(self, num, filename):
        if os.path.isdir(filename):
            return

        clean_filename = FILENAME_CLEAN_REGEX.sub('', filename, count=1)
        num_string = str(num).rjust(self.num_length, '0')
        new_filename = f'{num_string}. {clean_filename}'
        return new_filename

    def run(self):
        for num, filename in enumerate(self.files_list, start=1):
            new_filename = self.process_file(num, filename)
            self.processed_count += 1
            self.log_progress()

            if not new_filename:
                continue

            while self.task_queue.full():
                continue
            old_path = os.path.join(self.input_dir, filename)
            new_path = os.path.join(self.output_dir, new_filename)
            self.task_queue.put((old_path, new_path))

        self.running.value = c_bool(False)


def get_config():
    class Config(object):
        pass
    config = Config()
    config.input_dir = PATH
    config.output_dir = NEW_PATH
    return config


if __name__ == '__main__':
    cpu_count = multiprocessing.cpu_count()
    config = get_config()
    running = multiprocessing.Value(c_bool, True, lock=False)
    task_queue = multiprocessing.Queue(cpu_count)
    workers = []
    start = datetime.datetime.utcnow()
    print(f'Start shuffling files from "{config.input_dir}" to "{config.output_dir}"')
    for _ in range(cpu_count):
        worker = FileCopier(task_queue, running)
        worker.start()
        workers.append(worker)
    shuffler = FileShuffler(config, running, task_queue)
    shuffler.start()
    shuffler.join()
    for worker in workers:
        worker.join()
    end = datetime.datetime.utcnow()
    print(f'\nFinished in {end - start}')
